import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RestaurantPage } from './restaurant.page';
import { MapPage } from '../map/map.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/restaurant/tabs/find',
    pathMatch: 'full'
  },
  // {
  //   path: 'find',
  //   loadChildren: () => import('./find/find.module').then( m => m.FindPageModule)
  // }
  {
    path: 'tabs',               // /restaurant/tabs/
    component: RestaurantPage,
    children: [
      {
        path: 'find',           // /restaurant/tabs/find
        children: [
          {
            path: '',
            loadChildren: () => import('./find/find.module').then( m => m.FindPageModule)
          },
          {
            path: ':restaurantId',         // /restaurant/tabs/find/3
            loadChildren: () => import('./find/restaurant-detail/restaurant-detail.module').then( m => m.RestaurantDetailPageModule)
          }
        ]
      },
      {
        path: 'map',
        component: MapPage,
        loadChildren: () => import('../map/map.module').then( m => m.MapPageModule)
      },
      {
        path: '',
        redirectTo: '/restaurant/tabs/find',
        pathMatch: 'full'
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestaurantPageRoutingModule {}
