import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  // {
  //   path: 'home',           // localhost:xxxx/home
  //   loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  // },
  {
    path: '',                 // localhost:xxxx
    redirectTo: 'auth',
    pathMatch: 'full'
  },
  // {
  //   path: 'page2',          // localhost:xxxx/page2
  //   loadChildren: () => import('./page2/page2.module').then( m => m.Page2PageModule)
  // },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then( m => m.AuthPageModule)
  },
  // {
  //   path: 'map',
  //   loadChildren: () => import('./map/map.module').then( m => m.MapPageModule)
  // },
  {
    path: 'restaurant',         // localhost:xxxx/restaurant
    loadChildren: () => import('./restaurant/restaurant.module').then( m => m.RestaurantPageModule),
    canLoad: [AuthGuard]

  },
  {
    path: 'reservation',
    loadChildren: () => import('./reservation/reservation.module').then( m => m.ReservationPageModule),
    canLoad: [AuthGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
